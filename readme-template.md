<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your artifact name -->
# Artifact Name

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)

## Overview

<!-- Write a few sentences about the artifact and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->
<!--
<table><tr><td>
  <img src="images/workflow.png" alt="workflow" width="800px">
</td></tr></table>
-->
<!-- REPLACE COMMENT ABOVE WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_:

## Installation Prerequisites
In order to use the this artifact, users will have to satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2019.x.x`
* App-Artifacts
  * `^4.x.x`

## Requirements

<!-- Unordered list highlighting the requirements of the artifact -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
<!-- * Ansible or NSO (with F5 NED) * -->

## Features

<!-- Unordered list highlighting the most exciting features of the artifact -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->


## Future Enhancements

<!-- OPTIONAL - Mention if the artifact will be enhanced with additional features on the road map -->
<!-- Ex.: This artifact would support Cisco XR and F5 devices -->

## How to Install

Please ensure that you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the artifact. If you do not currently have App-Artifacts installed on your server, please download the installer from your Nexus repository. Please refer to the instructions included in the App-Artifacts README to install it.

The artifact can be installed from within App-Artifacts. Simply search for the name of your desired artifact and click the install button.

Alternatively, you may clone this repository and run `npm pack` to create a tarball which can then be installed via the offline installer in App-Artifacts. Please consult the documentation for App-Artifacts for further information.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this artifact can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

<!-- Explain the main entrypoint(s) for this artifact: Automation Catalog item, Workflow, Postman, etc. -->
