
## 2.4.11 [01-03-2024]

* Remove img tag from markdown files

See merge request itentialopensource/pre-built-automations/artifact-wizard!138

---

## 2.4.10 [10-19-2023]

* Adds deprecation notice with metadata file and updates Cypress dependency

See merge request itentialopensource/pre-built-automations/artifact-wizard!136

---

## 2.4.9 [06-21-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/artifact-wizard!130

---

## 2.4.8 [06-20-2022]

* patch/DSUP-1361

See merge request itentialopensource/pre-built-automations/artifact-wizard!120

---

## 2.4.7 [04-21-2022]

* changed service account token

See merge request itentialopensource/pre-built-automations/artifact-wizard!119

---

## 2.4.6 [02-14-2022]

* Patch/dsup 1046

See merge request itentialopensource/pre-built-automations/artifact-wizard!117

---

## 2.4.5 [11-09-2021]

* dsup-1104 - AW Broken on 2021.2

See merge request itentialopensource/pre-built-automations/artifact-wizard!116

---

## 2.4.4 [11-01-2021]

* re-added ops manager to form

See merge request itentialopensource/pre-built-automations/artifact-wizard!115

---

## 2.4.3 [10-25-2021]

* Updated regex for artifact name

See merge request itentialopensource/pre-built-automations/artifact-wizard!114

---

## 2.4.2 [10-01-2021]

* Update bundles/json_forms/Artifact Components.json

See merge request itentialopensource/pre-built-automations/artifact-wizard!113

---

## 2.4.1 [09-21-2021]

* Patch/dsup 1026

See merge request itentialopensource/pre-built-automations/artifact-wizard!109

---

## 2.4.0 [08-20-2021]

* Update bundles/workflows/IAP Artifacts AW Discover Components.json,...

See merge request itentialopensource/pre-built-automations/artifact-wizard!107

---

## 2.3.9 [07-26-2021]

* Update bundles/workflows/IAP Artifacts Artifact Wizard.json,...

See merge request itentialopensource/pre-built-automations/artifact-wizard!106

---

## 2.3.8 [07-01-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/artifact-wizard!105

---

## 2.3.7 [03-03-2021]

* Update bundles/workflows/IAP Artifacts AW GitLab Create Link.json

See merge request itentialopensource/pre-built-automations/artifact-wizard!103

---

## 2.3.6 [02-02-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/artifact-wizard!98

---

## 2.3.5 [02-02-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/artifact-wizard!95

---

## 2.3.4 [01-19-2021]

* Created MR for allowing users to edit their artifact scope

See merge request itentialopensource/pre-built-automations/artifact-wizard!94

---

## 2.3.3 [01-13-2021]

* Merge branch 'patch/naming_fields_in_form' into 'release/2020.1'

See merge request itentialopensource/pre-built-automations/artifact-wizard!93

---

## 2.3.2 [11-30-2020]

* Updated mentions of 'shllow' to 'shallow'

See merge request itentialopensource/pre-built-automations/artifact-wizard!91

---

## 2.3.1 [10-15-2020]

* path for release branch

See merge request itentialopensource/pre-built-automations/artifact-wizard!90

---

## 2.3.0 [10-15-2020]

* Minor/lb 475

See merge request itentialopensource/pre-built-automations/artifact-wizard!89

---

## 2.2.8 [09-21-2020]

* Update bundles/json_forms/Artifact Metadata.json, manifest.json files

See merge request itentialopensource/pre-built-automations/artifact-wizard!88

---

## 2.2.7 [09-21-2020]

* Update bundles/json_forms/Artifact Metadata.json, bundles/json_forms/Artifact...

See merge request itentialopensource/pre-built-automations/artifact-wizard!87

---

## 2.2.6 [08-21-2020]

* removed false form entry

See merge request itentialopensource/pre-built-automations/artifact-wizard!86

---

## 2.2.5 [08-05-2020]

* Added _id to fixPackageJson transformation

See merge request itentialopensource/pre-built-automations/artifact-wizard!84

---

## 2.2.4 [08-03-2020]

* Fixed Package.json

See merge request itentialopensource/pre-built-automations/artifact-wizard!83

---

## 2.2.3 [07-22-2020]

* Patch/remove manual tasks groups

See merge request itentialopensource/pre-built-automations/artifact-wizard!81

---

## 2.2.2 [07-13-2020]

* [patch/repoConvention] Add repo name change so user doesn't have to

See merge request itentialopensource/pre-built-automations/artifact-wizard!78

---

## 2.2.1 [07-10-2020]

* [patch/jsonform-upgrades] Remove limits from json forms calls

See merge request itentialopensource/pre-built-automations/artifact-wizard!77

---

## 2.2.0 [06-18-2020]

* [minor/LB-404] Switch absolute path to relative path for img

See merge request itentialopensource/pre-built-automations/artifact-wizard!74

---

## 2.1.0 [06-17-2020]

* Update manifest to reflect gitlab name

See merge request itentialopensource/pre-built-automations/Artifact-Wizard!73

---

## 2.0.0 [06-03-2020]

* [major/LB-334] Add additional functionality to artifact wizard

See merge request itentialopensource/pre-built-automations/artifact-wizard!71

---

## 1.0.13 [05-05-2020]

* 2020.1 specific changes

See merge request itentialopensource/pre-built-automations/artifact-wizard!70

---

## 1.0.12 [05-04-2020]

* Patch/2020.1 updates

See merge request itentialopensource/pre-built-automations/artifact-wizard!69

---

## 1.0.11 [05-01-2020]

* patch/README.md - Update

See merge request itentialopensource/pre-built-automations/artifact-wizard!68

---

## 1.0.10 [04-24-2020]

* Update IAP Artifacts AW Discover Components.json

See merge request itentialopensource/pre-built-automations/artifact-wizard!67

---

## 1.0.9 [04-23-2020]

* Update IAP Artifacts AW Discover Components.json

See merge request itentialopensource/pre-built-automations/artifact-wizard!66

---

## 1.0.8 [04-23-2020]

* Update Artifact Metadata.json

See merge request itentialopensource/pre-built-automations/artifact-wizard!65

---

## 1.0.7 [04-17-2020]

* Update bundles/json_forms/Artifact Components.json,...

See merge request itentialopensource/pre-built-automations/artifact-wizard!64

---

## 1.0.6 [04-17-2020]

* Update bundles/json_forms/Artifact Components.json,...

See merge request itentialopensource/pre-built-automations/artifact-wizard!64

---

## 1.0.5 [04-14-2020]

* Update bundles/json_forms/Artifact Components.json,...

See merge request itentialopensource/pre-built-automations/artifact-wizard!64

---

## 1.0.4 [04-14-2020]

* Update package.json

See merge request itentialopensource/pre-built-automations/artifact-wizard!63

---

## 1.0.3 [04-14-2020]

* Update Artifact Wizard Step 1.json

See merge request itentialopensource/pre-built-automations/artifact-wizard!61

---

## 1.0.2 [04-13-2020]

* Update package.json

See merge request itentialopensource/pre-built-automations/artifact-wizard!60

---

## 1.0.1 [04-03-2020]

* Add version and include Components json form

See merge request itentialopensource/pre-built-automations/artifact-wizard!57

---

## 1.0.0 [04-03-2020]

* [LB-273]Add discovery of multiple Automation Catalog items or workflows

See merge request itentialopensource/pre-built-automations/artifact-wizard!54

---

## 0.0.20 [03-30-2020]

* removed version from manifest.json

See merge request itentialopensource/pre-built-automations/artifact-wizard!55

---

## 0.0.19 [03-26-2020]

* Bring artifact wizard readme up to speed with latest changes

See merge request itentialopensource/pre-built-automations/artifact-wizard!48

---

## 0.0.18 [03-26-2020]

* Removed translations from all workflows; replaced with appropriate tasks

See merge request itentialopensource/pre-built-automations/artifact-wizard!50

---

## 0.0.17 [03-19-2020]

* Update readme template to reflect changes from artifact-template repository

See merge request itentialopensource/pre-built-automations/artifact-wizard!49

---

## 0.0.16 [03-06-2020]

* Remove Local AAA from all manual tasks

See merge request itentialopensource/pre-built-automations/artifact-wizard!45

---

## 0.0.15 [03-04-2020]

* Update app-artifacts requirement in package.json

See merge request itentialopensource/pre-built-automations/artifact-wizard!41

---

## 0.0.14 [02-06-2020]

* Update readme template to include commonly used instructions and placeholder images

See merge request itentialopensource/pre-built-automations/artifact-wizard!33

---

## 0.0.13 [02-03-2020]

* Migrate workflows and add improvements to show JSON form

See merge request itentialopensource/pre-built-automations/artifact-wizard!35

---

## 0.0.12 [01-21-2020]

* Update readme-template.md with TOC, headers, and examples

See merge request itentialopensource/pre-built-automations/artifact-wizard!30

---

## 0.0.11 [01-15-2020]

* Add readme template

See merge request itentialopensource/pre-built-automations/artifact-wizard!27

---

## 0.0.10 [01-07-2020]

* resize images in README.md

See merge request itentialopensource/pre-built-automations/artifact-wizard!24

---

## 0.0.9 [12-30-2019]

* Fix regex validation check for name on step 1 form

See merge request itentialopensource/pre-built-automations/artifact-wizard!23

---

## 0.0.8 [12-26-2019]

* Update package.json

See merge request itentialopensource/pre-built-automations/artifact-wizard!22

---

## 0.0.7 [12-23-2019]

* Update README.md

See merge request itentialopensource/pre-built-automations/artifact-wizard!21

---

## 0.0.6 [12-13-2019]

* allow artifact-wizard to create artifacts from a workflow entrypoint

See merge request itentialopensource/pre-built-automations/artifact-wizard!20

---

## 0.0.5 [12-09-2019]

* update README.md and add keyword of artifacts to package.json

See merge request itentialopensource/pre-built-automations/artifact-wizard!19

---

## 0.0.4 [12-06-2019]

* update repo information

See merge request itentialopensource/pre-built-automations/artifact-wizard!18

---

## 0.0.3 [12-06-2019]

* update repo information

See merge request itentialopensource/pre-built-automations/artifact-wizard!18

---

## 0.0.2 [12-06-2019]

* Updated README.md

See merge request itentialopensource/pre-built-automations/staging/artifact-wizard!11

---\n\n\n
